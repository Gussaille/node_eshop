const Mandatory = {
    register: [ 'givenName', 'familyName', 'password', 'email' ],
    login: [ 'password', 'email' ],
    comment: [ 'title', 'content' ],
    post: [ 'headline', 'body' ],

} 

module.exports = Mandatory;