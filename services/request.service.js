const checkFields = (required, bodyData) => {
    const miss = [];
    const extra = [];

    // Checking missing fields
    required.forEach((prop) => {
        if (!(prop in bodyData)) miss.push(prop);
    });

    // Checking extra fields
    for (const prop in bodyData) {
        if (required.indexOf(prop) === -1) extra.push(prop);
    }

    // Setting service state
    const ok = (extra.length === 0 && miss.length === 0);
    
    // Return service state
    return { ok, extra, miss };
}; 


module.exports = {
    checkFields
};