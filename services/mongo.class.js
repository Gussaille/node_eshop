const mongoose = require('mongoose');


class MONGOClass { 
    constructor(){
        // Set MongoDB url in env file
        this.mongoUrl = process.env.MONGO_URL;
    };

    connectDb(){
        return new Promise( (resolve, reject) => {
            mongoose.connect(this.mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true })
            .then( db => resolve( { db: db, url: this.mongoUrl } ))
            .catch( dbErr => reject(`MongoDB not connected`, dbErr) )
        });
    };
};

module.exports = MONGOClass;