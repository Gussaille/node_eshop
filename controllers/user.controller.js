/* 
Imports
*/
const Models = require('../models/index');
//

/*  
Methods
*/
    const readAll = () => {
        return new Promise( (resolve, reject) => {
            Models.user.find()
            .populate('author', [ '-password' ])
            .exec( (err, data) => {
                if( err ){ return reject(err) }
                else{ return resolve(data) }
            })
        })
    }

    const readOne = id => {
        return new Promise( (resolve, reject) => {
            Models.user.findById( id )
            .populate('author', [ 'password' ])
            .exec( (err, data) => {
                if( err ){ return reject(err) }
                else{ return resolve(data) }
            })
        })
    }

//

/* 
Export controller methods
*/
    module.exports = {
        readAll,
        readOne,
    }
//
