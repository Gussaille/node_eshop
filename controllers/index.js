const Controllers = {
    auth: require('./auth.controller'),
    user: require('./user.controller'),
    comment: require('./comment.controller'),
    post: require('./post.controller'),    
    like: require('./like.controller'),
}

module.exports = Controllers;
