const Models = {
    user: require('./user.model'),
    comment: require('./comment.model'),
    post: require('./post.model'),
    like: require('./like.model'),
} 

module.exports = Models;