const express = require('express');
const Controllers = require('../controllers/index')
const { checkFields } = require('../services/request.service');
const Mandatory = require('../services/mandatory.service');
const { renderSuccessVue, renderErrorVue } = require('../services/response.service');

class BackendRouter {
    constructor( { passport } ){
        this.passport = passport
        this.router = express.Router(); 
    } 

    routes(){
        // Defining index route
        this.router.get('/', this.passport.authenticate('jwt', { session: false, failureRedirect: '/login' }), (req, res) => {
            Controllers.post.readAll()
            .then( apiResponse => renderSuccessVue('index', '/', 'GET', res, 'Request succeed', apiResponse))
            .catch( apiError => renderErrorVue('index', '/', 'GET', res, 'Request failed', apiError) )
        })

        // Defining index route
        this.router.get('/login', (req, res) => {
            renderSuccessVue('login', '/login', 'GET', res, 'Request succeed', null)
        })

        this.router.post('/login', (req, res) => {
            // Check body data
            if( typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
                return res.json({ err: 'No data provided in the reqest body', data: null })
            }
            else{
                // Check body data
                const { ok, extra, miss } = checkFields( Mandatory.login, req.body );

                // Error
                if( !ok ){ return renderErrorVue('index', '/Login', 'POST', res, 'Bad fields provided', { extra, miss }) }
                else{
                    Controllers.auth.login(req, res)
                    .then( data => {
                        return renderSuccessVue('index', '/login', 'POST', res, 'User loged', data, true)
                    } )
                    .catch( err => {
                        return renderErrorVue('index', '/login', 'POST', res, 'User not loged', err, true);
                    } );
                }
            }
        })

        this.router.post('/:endpoint', this.passport.authenticate('jwt', { session: false, failureRedirect: '/' }), (req, res) => {
            // Check body data
            if( typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
                return res.json({ err: 'No data provided in the reqest body', data: null })
            }
            else{
                // Check body data
                const { ok, extra, miss } = checkFields( Mandatory[req.params.endpoint], req.body );

                // Error
                if( !ok ){ return renderErrorVue('index', `/${req.params.endpoint}`, 'POST', res, 'Bad fields provided', { extra, miss }) }
                else{
                    // Add author _id
                    req.body.author = req.user._id;

                    // Using the controller to create new object
                    Controllers[req.params.endpoint].createOne(req)
                    .then( apiResponse =>  res.redirect('/') )
                    .catch( apiError => renderErrorVue('index', `/${req.params.endpoint}`, 'POST', res, 'Request failed', apiError) )
                }
            }
        })
    }

    init(){
        // Get route fonctions
        this.routes();

        // Sendback router
        return this.router;
    };
}

module.exports = BackendRouter;